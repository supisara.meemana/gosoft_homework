package oop;

public class Employee {
	public String firstname;
	public String lastname;
	private int salary;
	public String position;
	public static void main(String[] args) {
		Employee em1 = new Employee("Supisara","Meemana",10000,"CEO");
		//em1.hellofriend("Ice");
		System.out.println(em1);
		
//		Employee[] employees = new Employee[3];
//		for(int i =0;i<employees.length;i++) {
//			employees[i] = new Employee("name"+i,"lastname"+i,i*10000,"CEO"+i);
//		}
//		System.out.println(employees[1].firstname);
//		System.out.println(employees[2].firstname);
//		System.out.println(employees[1].getSalary());
//		System.out.println(employees[2].getSalary());
	}
	@Override
	public String toString() {
		return "Firstname : "+this.firstname+ " Last name : "+this.lastname ;
	}
	public static void test() {
		System.out.println("This is static method");
	}


	public Employee(String firstnameInput, String lastnameInput,int salaryInput,String positionInput) {
		firstname = firstnameInput;
		lastname = lastnameInput;
		salary = salaryInput;
		position = positionInput;
	}
	public void hello() {
		System.out.println("Hello "+ this.firstname);
	}
	public int getSalary() {
		return salary;
	}
	public void checkposition() {
		System.out.println("My position is "+position);
	}
	public void hellofriend(String firstname) {
		System.out.println("I am "+this.firstname+" I want to meet u "+firstname);
	}
}
