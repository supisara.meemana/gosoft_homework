package oop;

public class Course {
	public String price;
	public String title;
	public String urlimage;
	public String soldtext;
	public int numtotal;
	public int numsold;
	public Course(String price, String title, String urlimage, String soldtext, int numtotal, int numsold) {
		super();
		this.price = price;
		this.title = title;
		this.urlimage = urlimage;
		this.soldtext = soldtext;
		this.numtotal = numtotal;
		this.numsold = numsold;
	}
	}
}
