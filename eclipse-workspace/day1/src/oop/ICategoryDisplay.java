package oop;

public interface ICategoryDisplay {
	String getUrlImage();
	String getDisplayName();

}
