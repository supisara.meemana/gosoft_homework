package HW3;

public class Programmer extends Employee{
    public int id;
    public String type;
    
    public Programmer(String firstnameInput,String lastnameInput,int salaryInput,int idInput,String typeInput) {
        super(firstnameInput, lastnameInput, salaryInput) ;
        this.id = idInput ;
        this.type = typeInput;
    }
}
