package HW3;

public class Homework9_3 {
    public static void main(String[] args) {
        String[] rawData = {
            "id:1001 firstname:Luke lastname:Skywalker salary:10000 type:frontend",
            "id:1002 firstname:Tony lastname:Stark salary:20000 type:backend",
            "id:1003 firstname:Somchai lastname:Jaidee salary:30000 type:fullstack",
            "id:1004 firstname:MonkeyD lastname:Luffee salary:40000 type:fullstack"
        };
        CEO ceo = new CEO("First", "Last", 1000000000);
        data(rawData,ceo);
        for(Programmer var : ceo.employess){
            System.out.println(var.firstname);
        }
    }
    public static void data(String[] rawdata,CEO ceo) {
        Programmer[] programmers = new Programmer[4];
        for(int i = 0; i < rawdata.length; i++) {
            String[] split1 = rawdata[i].split(" ") ;
            String[] name = new String[5];
            for(int index = 0; index < split1.length; index++) {
                String split2 = split1[index].split(":")[1] ;
                name[index] = split2;
            }
            programmers[i] = new Programmer(name[1], name[2], Integer.parseInt(name[3]), Integer.parseInt(name[0]), name[4]);
        }
        ceo.employess = programmers;
    }
}
